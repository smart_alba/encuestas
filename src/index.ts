import * as dotenv from "dotenv";

import { Log } from "./util/log";
import { OpiClass } from "./api/opi";
import { RequestService } from "./util/request";
import { FileUtil } from "./util/file";


dotenv.config({path: ".env"});

const config = {
    domain: process.env.DOMAIN,
    name: process.env.NAME,
    token: process.env.TOKEN,
    key: process.env.KEY
};

function getOpis() {
    return new Promise(async (resolve, reject) => {
        const opis: any = await OpiClass.getOpis(config);
        try {
            await FileUtil.saveFile('encuestas.csv', opis);
        } catch (err) {
            Log.error(err, "MAIN", 'Execute Tasks - Save Opis');
        }
        resolve(opis);
    });
}

function getOpinions() {
    return new Promise(async (resolve, reject) => {
        let opinion: any[] = [];
        const opis: any = await OpiClass.getOpis(config);
        for (const opi of opis) {
            try {
                await RequestService.delay(10500);
                const data: any = await OpiClass.getOpinions(config, opi.id);
                opinion = [...opinion, ...data];
            } catch (err) {
                Log.error(err, "MAIN", 'Execute Tasks - id: ' + opi.id);
            }
        }
        try {
            await FileUtil.saveFile('opiniones.csv', opinion);
        } catch (err) {
            Log.error(err, "MAIN", 'Execute Tasks - Save Opiniones');
        }
        resolve(opinion);
    });
}

function tasks() {
    getOpis().then((data: any) => {
        console.log("Encuestas totales: " + data.length);
    }).catch((err: any) => {
        Log.error(err, "MAIN", 'Call Tasks');
    });
    getOpinions().then((data: any) => {
        console.log("Opiniones totales: " + data.length);
    }).catch((err: any) => {
        Log.error(err, "MAIN", 'Call Tasks');
    });
}

tasks();