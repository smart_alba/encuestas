import * as CryptoJS from "crypto-js";
import { RequestService } from "../util/request";
import { Log } from "../util/log";

class Opi {
    class = 'OpiClass';

    constructor() {
    }

    public async getOpis(p: any) {
        return new Promise(async (resolve, reject) => {
            const params = this.getParams(p.token, p.key, p.name);
            const url = p.domain + '/external-api/api/v2/opis/find' + params;
            try {
                const data: any = await RequestService.GET(url);
                const opis = data.responseObject.map(this.cleanOpis);
                resolve(opis);
            } catch (err) {
                Log.error(err, this.class, 'getOpis');
                reject([]);
            }
        });
    }

    public async getOpinions(p: any, id: string) {
        return new Promise(async (resolve, reject) => {
            const params = this.getParams(p.token, p.key, p.name);
            const url = p.domain + '/external-api/api/v2/opinions/find' + params + "&opi=" + id;
            try {
                const data: any = await RequestService.GET(url);
                const opinions = data.responseObject && data.responseObject.map((opinion: any) => {
                    return this.cleanOpinion(opinion, id);
                });
                console.log("Opiniones para " + id + ": ", opinions.length);
                resolve(opinions);
            } catch (err) {
                Log.error(err, this.class, 'getOpinions');
                reject([]);
            }
        });
    }

    private getParams(token: string, key: string, name: string) {
        const ts = new Date().getTime();
        const params = "ts=" + ts + "&token=" + token;
        const signature = encodeURIComponent(CryptoJS.enc.Base64.stringify(CryptoJS.HmacSHA256(params, key)));
        return '?ts=' + ts + "&token=" + token + "&sig=" + signature + "&name=" + name;
    }

    private cleanOpis(opi: any) {
        let _o = opi;
        delete _o.image_url;
        delete _o.lang;
        return _o;
    }

    private cleanOpinion(opinion: any, id: string) {
        let _o = opinion;
        _o.carry_opisense = opinion.carry && opinion.carry.carry_opisense || 0;
        _o.encuesta = id;
        delete _o.answers;
        delete _o.carry;
        return _o;
    }
}

export let OpiClass = new Opi();
