import * as request from "request";
import { Log } from "./log";

class Service {
    class = "RequestService";

    constructor() {
    }

    public async GET(url: string) {
        return this._request('GET', url);
    }

    public async POST(url: string, body: any) {
        return this._request('POST', url, body);
    }

    public async delay(milliseconds: number): Promise<number> {
        return new Promise<number>(resolve => {
            setTimeout(() => {
                resolve();
            }, milliseconds);
        });
    }

    private async _request(type: string, url: string, body: any = {}) {
        let req: any = {
            url: url,
            method: type,
            json: true,
            gzip: true
        };
        if (type === "POST") {
            req.body = body;
        }
        return new Promise(async (resolve, reject) => {
            request(req, (err: any, httpResponse: any, body: any) => {
                if (err) {
                    const e = err || 'api server error - ' + httpResponse.statusCode;
                    Log.error(e, this.class, 'Request');
                    reject(e);
                    return;
                }
                return resolve(body);
            });
        });
    }
}

export let RequestService = new Service();