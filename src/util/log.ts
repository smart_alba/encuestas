export class Log {
    public static error(err: string, clase: string, method: string) {
        console.log("\x1b[33m%s\x1b[0m", "[Alba > " + clase + " > " + method + "] - Error: " + err);
    }
}